package com.ftd.services.carrier.integration.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=false)
@Document(collection = "memberCarrierDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberCarrierDetails extends Audit {

    @Id
    private String memberCarrierDetailsId;
    private String siteId;
    private String memberNo;
    private String memberCategory;
    private List<TrackingServiceDetails> trackingServiceDetails;
}
