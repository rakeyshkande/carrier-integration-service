package com.ftd.services.carrier.integration.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.commons.misc.exception.NoRecordsFoundException;
import com.ftd.services.carrier.integration.constants.AppConstants;
import com.ftd.services.carrier.integration.entity.CalyxCarrierEvent;
import com.ftd.services.carrier.integration.repository.CalyxCarrierEventDao;
import com.ftd.services.carrier.integration.service.CalyxCarrierEventService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CalyxCarrierEventServiceImpl implements CalyxCarrierEventService {

    @Autowired
    CalyxCarrierEventDao calyxCarrierEventDao;

    @Override
    public List<CalyxCarrierEvent> getCalyxCarrierEventsByCarrier(String siteId, String carrierId, String carrierEvent) {
        log.info("Invoked CalyxCarrierEventServiceImpl:getCalyxCarrierEventDetails method");
        Optional<List<CalyxCarrierEvent>> calyxCarrierEvents = calyxCarrierEventDao.getCalyxCarrierEventDetailsByCarrier(siteId, carrierId, carrierEvent);
        if (calyxCarrierEvents.isPresent()) {
            return calyxCarrierEvents.get();
        } else {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND,
                    "No Calyx Carrier Events found for the given site id - " + siteId, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public CalyxCarrierEvent getCalyxCarrierEventById(String siteId, String calyxCarrierEventId) {
        log.info("Invoked CalyxCarrierEventServiceImpl:getCalyxCarrierEventsById method");
        Optional<CalyxCarrierEvent> calyxCarrierEvent = calyxCarrierEventDao.getCalyxCarrierEventDetailsById(siteId, calyxCarrierEventId);
        if (calyxCarrierEvent.isPresent()) {
            return calyxCarrierEvent.get();
        } else {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND,
                    "No Calyx Carrier Events found for the given site id - " + siteId, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public CalyxCarrierEvent saveCalyxCarrierEvent(String siteId, CalyxCarrierEvent calyxCarrierEvent) {
        log.info("Invoked CalyxCarrierEventServiceImpl:saveCalyxCarrierEvent method");
        calyxCarrierEvent.setSiteId(siteId);
        calyxCarrierEvent.setCalyxCarrierEventId(null);
        calyxCarrierEvent.setCreatedDate(new Date());
        calyxCarrierEvent.setModifiedDate(new Date());
        Optional<CalyxCarrierEvent> existingCalyxCarrierEvent = calyxCarrierEventDao
                .getCalyxCarrierEventByCarrierIdAndCarrierEventAndCalyxEvent(siteId, calyxCarrierEvent.getCarrierId(),
                        calyxCarrierEvent.getCarrierEvent(), calyxCarrierEvent.getCalyxEvent());
        if (existingCalyxCarrierEvent.isPresent()) {
            throw new InputValidationException(AppConstants.RESOURCE_ALREADY_EXISTS, "Resource already exists with these details", HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            Optional<CalyxCarrierEvent> savedCalyxCarrierEvent = calyxCarrierEventDao.saveCalyxCarrierEvent(calyxCarrierEvent);
            log.info("Inserted Member Carrier Details - {}", savedCalyxCarrierEvent.get().getCalyxCarrierEventId());
            return savedCalyxCarrierEvent.get();
        }
    }

    @Override
    public String deleteCalyxCarrierEventById(String siteId, String calyxCarrierEventId) {
        Optional<CalyxCarrierEvent> calyxCarrierEvent = calyxCarrierEventDao.getCalyxCarrierEventDetailsById(siteId, calyxCarrierEventId);
        if (!calyxCarrierEvent.isPresent()) {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site & id - " + siteId + calyxCarrierEventId, HttpStatus.NOT_FOUND);
        }
        calyxCarrierEventDao.deleteCalyxCarrierEvent(siteId, calyxCarrierEventId);
        return "SUCCESS";
    }

}
