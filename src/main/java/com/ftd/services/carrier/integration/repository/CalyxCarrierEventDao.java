package com.ftd.services.carrier.integration.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import com.ftd.services.carrier.integration.entity.CalyxCarrierEvent;

@Repository
public class CalyxCarrierEventDao extends BaseDaoImpl {

    @Autowired
    MongoOperations mongoOperations;

    public Optional<List<CalyxCarrierEvent>> getCalyxCarrierEventDetailsByCarrier(String siteId, String carrierId,
            String carrierEvent) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQuerySiteId()).is(siteId).and(getQueryCarrierId()).is(carrierId));
        if (!ObjectUtils.isEmpty(carrierEvent)) {
            query.addCriteria(Criteria.where(getQueryCarrierEvent()).is(carrierEvent));
        }
        List<CalyxCarrierEvent> calyxCarrierEvents = mongoOperations.find(query, CalyxCarrierEvent.class);
        return Optional.ofNullable(calyxCarrierEvents);
    }

    public Optional<CalyxCarrierEvent> getCalyxCarrierEventDetailsById(String siteId, String calyxCarrierEventId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQueryId()).is(calyxCarrierEventId).and(getQuerySiteId()).is(siteId));
        CalyxCarrierEvent calyxCarrierEvent = mongoOperations.findOne(query, CalyxCarrierEvent.class);
        return Optional.ofNullable(calyxCarrierEvent);
    }

    public Optional<CalyxCarrierEvent> getCalyxCarrierEventByCarrierIdAndCarrierEventAndCalyxEvent(String siteId,
            String carrierId, String carrierEvent, String calyxEvent) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQueryCarrierEvent()).is(carrierEvent).and(getQueryCalyxEvent())
                .is(calyxEvent).and(getQueryCarrierId()).is(carrierId).and(getQuerySiteId()).is(siteId));
        CalyxCarrierEvent calyxCarrierEvent = mongoOperations.findOne(query, CalyxCarrierEvent.class);
        return Optional.ofNullable(calyxCarrierEvent);
    }

    public Optional<CalyxCarrierEvent> saveCalyxCarrierEvent(CalyxCarrierEvent calyxCarrierEvent) {
        mongoOperations.save(calyxCarrierEvent);
        return Optional.ofNullable(calyxCarrierEvent);
    }

    public void deleteCalyxCarrierEvent(String siteId, String calyxCarrierEventId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQueryId()).is(calyxCarrierEventId).and(getQuerySiteId()).is(siteId));
        mongoOperations.remove(query, CalyxCarrierEvent.class);
    }
    
}
