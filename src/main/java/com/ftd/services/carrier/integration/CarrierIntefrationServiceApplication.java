package com.ftd.services.carrier.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication(scanBasePackages= {"com.ftd"})
public class CarrierIntefrationServiceApplication {

	public static void main(String[] args) {
	    log.info("Starting Spring boot application - Carrier integration Microservices");
	    SpringApplication.run(CarrierIntefrationServiceApplication.class, args);
	}

}
