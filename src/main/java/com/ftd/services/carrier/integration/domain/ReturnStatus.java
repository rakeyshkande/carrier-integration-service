package com.ftd.services.carrier.integration.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReturnStatus implements Serializable {

    private static final long serialVersionUID = 4976474134772335901L;

    @ApiModelProperty(notes = "Status of the API call", example = "true/false")
    private boolean success;
    @ApiModelProperty(notes = "Unique identifier for the resource", example = "customer id or address id etc.")
    private String id;
    @ApiModelProperty(notes = "Information about the status of API call")
    private String message;
    @ApiModelProperty(notes = "Total customer membership savings", hidden = true)
    private String totalSavings;
    @ApiModelProperty(notes = "Current active customer membership savings", hidden = true)
    private String currentSavings;

    private List<String> errorMessages;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalSavings() {
        return this.totalSavings;
    }

    public void setTotalSavings(String totalSavings) {
        this.totalSavings = totalSavings;
    }

    public String getCurrentSavings() {
        return this.currentSavings;
    }

    public void setCurrentSavings(String currentSavings) {
        this.currentSavings = currentSavings;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public static final int PRIME_NO_1 = 1231;
    public static final int PRIME_NO_2 = 1237;

    @Override
    public String toString() {
        return "Status [success=" + success + ", id=" + id + ", message=" + message + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        if (currentSavings == null) {
            result = prime * result + 0;
        } else {
            result = prime * result + currentSavings.hashCode();
        }
        if (id == null) {
            result = prime * result + 0;
        } else {
            result = prime * result + id.hashCode();
        }
        if (message == null) {
            result = prime * result + 0;
        } else {
            result = prime * result + message.hashCode();
        }
        if (success) {
            result = prime * result + PRIME_NO_1;
        } else {
            result = prime * result + PRIME_NO_2;
        }
        if (totalSavings == null) {
            result = prime * result + 0;
        } else {
            result = prime * result + totalSavings.hashCode();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ReturnStatus other = (ReturnStatus) obj;
        if (currentSavings == null) {
            if (other.currentSavings != null) {
                return false;
            }
        } else if (!currentSavings.equals(other.currentSavings)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!message.equals(other.message)) {
            return false;
        }
        if (success != other.success) {
            return false;
        }
        if (totalSavings == null) {
            if (other.totalSavings != null) {
                return false;
            }
        } else if (!totalSavings.equals(other.totalSavings)) {
            return false;
        }
        return true;
    }

}
