package com.ftd.services.carrier.integration.service;

import java.util.Map;

import org.springframework.stereotype.Service;

public interface GlobalSchedulerService {

    public void process(String startMsg, Map<String, String> localHeaders, String message);

    public void startScheduler();
}
