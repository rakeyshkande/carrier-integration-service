package com.ftd.services.carrier.integration.pubsub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.carrier.integration.config.Pubsubconfig;
import com.ftd.services.carrier.integration.entity.MemberCarrierDetails;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GlobalSchedulerPublisher {

    @Autowired
    private PublisherUtil publisherUtil;

    @Autowired
    private Pubsubconfig pubsubconfig;

    public void publish(MemberCarrierDetails message) {
        Gson gson = new Gson();
        String msgData = gson.toJson(message);
        String globalSchedulerTopic = pubsubconfig.getPubSub().getGlobalSchedulerTopic();
        publisherUtil.publish(msgData, globalSchedulerTopic);
        log.info("Event has been published with message {} to {} topic.", msgData, message);
    }

}
