package com.ftd.services.carrier.integration.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=false)
@Document(collection = "carrierDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarrierDetails extends Audit {

    @Id
    private String carrierDetailsId;
    private String siteId;
    private Integer carrierId;
    private String carrierName;
    private String description;
    private String shortName;
    private String trackingLink;
    private List<ServiceType> serviceTypes;
}
