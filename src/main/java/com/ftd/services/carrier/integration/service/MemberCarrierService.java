package com.ftd.services.carrier.integration.service;

import java.util.List;

import com.ftd.services.carrier.integration.entity.MemberCarrierDetails;

public interface MemberCarrierService {

    List<MemberCarrierDetails> getMemberCarrierDetails(String siteId);

    MemberCarrierDetails getMemberCarrierDetails(String siteId, String referenceNo, String type);

    MemberCarrierDetails saveMemberCarrierDetails(String siteId, MemberCarrierDetails memberCarrier);

    MemberCarrierDetails updateMemberCarrierDetails(String siteId, String memberNo, MemberCarrierDetails memberCarrier);

    String deleteOrderTrackingDetailsById(String siteId, String memberCarrierDetailsId, String carrierId);

    String deleteOrderTrackingDetailsByMemberNo(String siteId, String memberNo, String carrierId);

}
