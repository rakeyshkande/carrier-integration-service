package com.ftd.services.carrier.integration.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ServiceType {

    private String type;
    private String description;
    private String dispatchLeadDays;
}
