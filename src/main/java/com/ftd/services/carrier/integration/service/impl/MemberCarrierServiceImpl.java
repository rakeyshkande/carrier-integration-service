package com.ftd.services.carrier.integration.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.commons.misc.exception.NoRecordsFoundException;
import com.ftd.services.carrier.integration.constants.AppConstants;
import com.ftd.services.carrier.integration.entity.MemberCarrierDetails;
import com.ftd.services.carrier.integration.entity.TrackingServiceDetails;
import com.ftd.services.carrier.integration.repository.MemberCarrierDao;
import com.ftd.services.carrier.integration.service.MemberCarrierService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MemberCarrierServiceImpl implements MemberCarrierService {

    @Autowired
    private MemberCarrierDao memberCarrierDao;;

    @Override
    public List<MemberCarrierDetails> getMemberCarrierDetails(String siteId) {
        log.info("Invoked MemberCarrierServiceImpl:getMemberCarrierDetails method");
        Optional<List<MemberCarrierDetails>> memberCarriers = memberCarrierDao.getMemberCarrierDetails(siteId);
        if (memberCarriers.isPresent()) {
            return memberCarriers.get();
        } else {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site id - " + siteId, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public MemberCarrierDetails getMemberCarrierDetails(String siteId, String referenceNo, String type) {
        MemberCarrierDetails memberCarrier = null;
        if ("ID".equalsIgnoreCase(type)) {
            memberCarrier = this.getMemberCarrierDetailsById(siteId, referenceNo);
        } else if ("MEMBER".equalsIgnoreCase(type)) {
            memberCarrier = this.getMemberCarrierDetailsByMemberNo(siteId, referenceNo);
        }
        return memberCarrier;
    }

    public MemberCarrierDetails getMemberCarrierDetailsById(String siteId, String memberCarrierDetailsId) {
        log.info("Invoked MemberCarrierServiceImpl:getMemberCarrierDetailsById method");
        Optional<MemberCarrierDetails> memberCarrier = memberCarrierDao.getMemberCarrierDetailsById(siteId, memberCarrierDetailsId);
        if (memberCarrier.isPresent()) {
            return memberCarrier.get();
        } else {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site & id - " + siteId + memberCarrierDetailsId, HttpStatus.NOT_FOUND);
        }
    }

    public MemberCarrierDetails getMemberCarrierDetailsByMemberNo(String siteId, String memberNo) {
        log.info("Invoked MemberCarrierServiceImpl:getMemberCarrierDetailsByMemberNo method");
        Optional<MemberCarrierDetails> memberCarrier = memberCarrierDao.getMemberCarrierDetailsByMemberNo(siteId, memberNo);
        if (memberCarrier.isPresent()) {
            return memberCarrier.get();
        } else {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site id & member no - " + siteId + memberNo, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public MemberCarrierDetails saveMemberCarrierDetails(String siteId, MemberCarrierDetails memberCarrier) {
        log.info("Invoked MemberCarrierServiceImpl:saveMemberCarrierDetails method");
        memberCarrier.setSiteId(siteId);
        memberCarrier.setMemberCarrierDetailsId(null);
        memberCarrier.setCreatedDate(new Date());
        memberCarrier.setModifiedDate(new Date());
        Optional<MemberCarrierDetails> existingMemberCarrier = memberCarrierDao.getMemberCarrierDetailsByMemberNo(siteId, memberCarrier.getMemberNo());
        if (existingMemberCarrier.isPresent()) {
            throw new InputValidationException(AppConstants.RESOURCE_ALREADY_EXISTS, "Resource already exists with these details", HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            Optional<MemberCarrierDetails> savedMemberCarrier = memberCarrierDao.saveMemberCarrierDetails(memberCarrier);
            log.info("Inserted Member Carrier Details - {}", savedMemberCarrier.get().getMemberCarrierDetailsId());
            return savedMemberCarrier.get();
        }
    }

    @Override
    public MemberCarrierDetails updateMemberCarrierDetails(String siteId, String memberNo,
            MemberCarrierDetails memberCarrier) {
        log.info("Invoked MemberCarrierServiceImpl:updateMemberCarrierDetails method");
        Optional<MemberCarrierDetails> existingMemberCarrier = memberCarrierDao.getMemberCarrierDetailsByMemberNo(siteId, memberNo);
        if (existingMemberCarrier.isPresent()) {
            List<Integer> carrierIds = memberCarrier.getTrackingServiceDetails().stream().map(TrackingServiceDetails::getCarrierId).collect(Collectors.toList());
            Optional<MemberCarrierDetails> savedMemberCarrier = memberCarrierDao.updateMemberCarrierDetails(siteId, memberCarrier, carrierIds.toArray(new Integer[0]));
            if (savedMemberCarrier.isPresent()) {
                log.info("Inserted Member Carrier Details - {}", savedMemberCarrier.get().getMemberCarrierDetailsId());
                return savedMemberCarrier.get();
            } else {
                throw new InputValidationException(AppConstants.UNPROCESSABLE_ENTITY, "Unable to save resource with these details", HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } else {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site id & member no - " + siteId + memberNo, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public String deleteOrderTrackingDetailsById(String siteId, String memberCarrierDetailsId,
            String carrierId) {
        Optional<MemberCarrierDetails> memberCarrier = memberCarrierDao.getMemberCarrierDetailsById(siteId, memberCarrierDetailsId);
        if (!memberCarrier.isPresent()) {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site & id - " + siteId + memberCarrierDetailsId, HttpStatus.NOT_FOUND);
        }
        if (!ObjectUtils.isEmpty(carrierId)) {
            Optional<MemberCarrierDetails> savedMemberCarrier = memberCarrierDao.removeMemberCarrier(siteId, memberCarrierDetailsId, carrierId, "ID");
            if (savedMemberCarrier.isPresent()) {
                log.info("Removed Member Carrier Details - {}", savedMemberCarrier.get().getMemberCarrierDetailsId());
                return savedMemberCarrier.get().getMemberCarrierDetailsId();
            } else {
                return "FAILURE";
            }
        } else {
            memberCarrierDao.deleteByMemberCarrierDetails(siteId, memberCarrierDetailsId, "ID");
            return "SUCCESS";
        }
    }

    @Override
    public String deleteOrderTrackingDetailsByMemberNo(String siteId, String memberNo, String carrierId) {
        Optional<MemberCarrierDetails> memberCarrier = memberCarrierDao.getMemberCarrierDetailsByMemberNo(siteId, memberNo);
        if (!memberCarrier.isPresent()) {
            throw new NoRecordsFoundException(AppConstants.RESOURCE_NOT_FOUND, "No members found for the given site & member no - " + siteId + memberNo, HttpStatus.NOT_FOUND);
        }
        if (!ObjectUtils.isEmpty(carrierId)) {
            Optional<MemberCarrierDetails> savedMemberCarrier = memberCarrierDao.removeMemberCarrier(siteId, memberNo, carrierId, "MEMBER_NO");
            if (savedMemberCarrier.isPresent()) {
                log.info("Removed Member Carrier Details - {}", savedMemberCarrier.get().getMemberCarrierDetailsId());
                return savedMemberCarrier.get().getMemberCarrierDetailsId();
            } else {
                return "FAILURE";
            }
        } else {
            memberCarrierDao.deleteByMemberCarrierDetails(siteId, memberNo, "MEMBER_NO");
            return "SUCCESS";
        }
    }
    
}
