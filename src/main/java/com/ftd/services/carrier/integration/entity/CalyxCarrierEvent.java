package com.ftd.services.carrier.integration.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=false)
@Document(collection = "calyxCarrierEventDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalyxCarrierEvent extends Audit {

    @Id
    private String calyxCarrierEventId;
    private String siteId;
    private String carrierId;
    private String carrierEvent;
    private String carrierEventDescription;
    private String calyxEvent;
    private String calyxEventDescriotion;
}
