package com.ftd.services.carrier.integration.constants;

public final class AppConstants {

    private AppConstants() {
    }

    public static final String DEFAULT_SUCCESS_MESSAGE = "Request has been processed successfully";
    public static final String DEFAULT_FAILURE_MESSAGE = "Error occurred while processing the request";

    public static final String RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
    public static final String UNPROCESSABLE_ENTITY = "UNPROCESSABLE_ENTITY";
    public static final String RESOURCE_ALREADY_EXISTS = "RESOURCE_ALREADY_EXISTS";
    //public static final String  = "";

}
