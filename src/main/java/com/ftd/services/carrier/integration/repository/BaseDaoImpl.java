package com.ftd.services.carrier.integration.repository;

public class BaseDaoImpl {

    private static String queryId = "_id";
    private static String querySiteId = "siteId";
    private static String queryMemberNo = "memberNo";
    private static String queryTrackingServiceDetails = "trackingServiceDetails";
    private static String queryCarrierId = "carrierId";
    private static String queryCarrierEvent = "carrierEvent";
    private static String queryCalyxEvent = "calyxEvent";
    
    private static String queryModifiedDate = "modifiedDate";
    private static String queryModifiedBy = "modifiedBy";

    public static String getQueryId() {
        return queryId;
    }
    public static void setQueryId(String queryId) {
        BaseDaoImpl.queryId = queryId;
    }
    public static String getQuerySiteId() {
        return querySiteId;
    }
    public static void setQuerySiteId(String querySiteId) {
        BaseDaoImpl.querySiteId = querySiteId;
    }
    public static String getQueryMemberNo() {
        return queryMemberNo;
    }
    public static void setQueryMemberNo(String queryMemberNo) {
        BaseDaoImpl.queryMemberNo = queryMemberNo;
    }
    public static String getQueryTrackingServiceDetails() {
        return queryTrackingServiceDetails;
    }
    public static void setQueryTrackingServiceDetails(String queryTrackingServiceDetails) {
        BaseDaoImpl.queryTrackingServiceDetails = queryTrackingServiceDetails;
    }
    public static String getQueryCarrierId() {
        return queryCarrierId;
    }
    public static void setQueryCarrierId(String queryCarrierId) {
        BaseDaoImpl.queryCarrierId = queryCarrierId;
    }
    public static String getQueryModifiedDate() {
        return queryModifiedDate;
    }
    public static void setQueryModifiedDate(String queryModifiedDate) {
        BaseDaoImpl.queryModifiedDate = queryModifiedDate;
    }
    public static String getQueryModifiedBy() {
        return queryModifiedBy;
    }
    public static void setQueryModifiedBy(String queryModifiedBy) {
        BaseDaoImpl.queryModifiedBy = queryModifiedBy;
    }
    public static String getQueryCarrierEvent() {
        return queryCarrierEvent;
    }
    public static void setQueryCarrierEvent(String queryCarrierEvent) {
        BaseDaoImpl.queryCarrierEvent = queryCarrierEvent;
    }
    public static String getQueryCalyxEvent() {
        return queryCalyxEvent;
    }
    public static void setQueryCalyxEvent(String queryCalyxEvent) {
        BaseDaoImpl.queryCalyxEvent = queryCalyxEvent;
    }

}
