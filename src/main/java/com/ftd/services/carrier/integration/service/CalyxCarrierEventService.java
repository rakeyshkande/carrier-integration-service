package com.ftd.services.carrier.integration.service;

import java.util.List;

import com.ftd.services.carrier.integration.entity.CalyxCarrierEvent;

public interface CalyxCarrierEventService {

    List<CalyxCarrierEvent> getCalyxCarrierEventsByCarrier(String siteId, String carrierId, String carrierEvent);

    CalyxCarrierEvent getCalyxCarrierEventById(String siteId, String calyxCarrierEventId);

    CalyxCarrierEvent saveCalyxCarrierEvent(String siteId, CalyxCarrierEvent calyxCarrierEvent);

    String deleteCalyxCarrierEventById(String siteId, String calyxCarrierEventId);

}
