package com.ftd.services.carrier.integration.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "service")
@EnableConfigurationProperties
public class Pubsubconfig {

    private PubSub pubSub;

    public PubSub getPubSub() {
        return pubSub;
    }

    public void setPubSub(PubSub pubSub) {
        this.pubSub = pubSub;
    }

    public static class PubSub {

        private String key;
        private String projectId;
        private long publisherTimeOut;
        private String globalSchedulerTopic;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getProjectId() {
            return projectId;
        }

        public void setProjectId(String projectId) {
            this.projectId = projectId;
        }

        public long getPublisherTimeOut() {
            return publisherTimeOut;
        }

        public void setPublisherTimeOut(long publisherTimeOut) {
            this.publisherTimeOut = publisherTimeOut;
        }

        public String getGlobalSchedulerTopic() {
            return globalSchedulerTopic;
        }

        public void setGlobalSchedulerTopic(String globalSchedulerTopic) {
            this.globalSchedulerTopic = globalSchedulerTopic;
        }

    }
}
