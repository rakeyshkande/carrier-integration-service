package com.ftd.services.carrier.integration.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.ftd.services.carrier.integration.entity.MemberCarrierDetails;
import com.mongodb.BasicDBObject;

@Repository
public class MemberCarrierDao extends BaseDaoImpl {

    @Autowired
    MongoOperations mongoOperations;

    public Optional<List<MemberCarrierDetails>> getMemberCarrierDetails(String siteId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQuerySiteId()).is(siteId));
        List<MemberCarrierDetails> memberCarriers = mongoOperations.find(query, MemberCarrierDetails.class);
        return Optional.ofNullable(memberCarriers);
    }

    public Optional<MemberCarrierDetails> getMemberCarrierDetailsById(String siteId, String memberCarrierDetailsId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQueryId()).is(memberCarrierDetailsId).and(getQuerySiteId()).is(siteId));
        MemberCarrierDetails memberCarriers = mongoOperations.findOne(query, MemberCarrierDetails.class);
        return Optional.ofNullable(memberCarriers);
    }

    public Optional<MemberCarrierDetails> getMemberCarrierDetailsByMemberNo(String siteId, String memberNo) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQueryMemberNo()).is(memberNo).and(getQuerySiteId()).is(siteId));
        MemberCarrierDetails memberCarriers = mongoOperations.findOne(query, MemberCarrierDetails.class);
        return Optional.ofNullable(memberCarriers);
    }

    public Optional<MemberCarrierDetails> saveMemberCarrierDetails(MemberCarrierDetails memberCarrier) {
        mongoOperations.save(memberCarrier);
        return Optional.ofNullable(memberCarrier);
    }

    public Optional<MemberCarrierDetails> updateMemberCarrierDetails(String siteId,
            MemberCarrierDetails memberCarrier, Integer[] carrierIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(getQueryMemberNo()).is(memberCarrier.getMemberNo()).and(getQuerySiteId()).is(siteId));
        if (!CollectionUtils.sizeIsEmpty(carrierIds)) {
            Update updateObj = new Update();
            for (Integer carrierId:carrierIds) {
                updateObj.pull(getQueryTrackingServiceDetails(), new BasicDBObject(getQueryCarrierId(), carrierId));
                FindAndModifyOptions options = new FindAndModifyOptions();
                mongoOperations.findAndModify(query, updateObj, options, MemberCarrierDetails.class);
            }
        }
        Update updateObj = new Update();
        updateObj.push(getQueryTrackingServiceDetails()).each(memberCarrier.getTrackingServiceDetails().toArray());
        updateObj.set(getQueryModifiedDate(), new Date());
        updateObj.set(getQueryModifiedBy(), memberCarrier.getModifiedBy());
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);
        return Optional.ofNullable(mongoOperations.findAndModify(query, updateObj, options, MemberCarrierDetails.class));
    }

    public Optional<MemberCarrierDetails> removeMemberCarrier(String siteId, String queryValue,
            String carrierId, String type) {
        Query query = new Query();
        if ("ID".equals(type)) {
            query.addCriteria(Criteria.where(getQueryId()).is(queryValue).and(getQuerySiteId()).is(siteId));
        } else if ("MEMBER_NO".equals(type)) {
            query.addCriteria(Criteria.where(getQueryMemberNo()).is(queryValue).and(getQuerySiteId()).is(siteId));
        }
        Update updateObj = new Update();
        updateObj.pull(getQueryTrackingServiceDetails(), new BasicDBObject(getQueryCarrierId(), carrierId));
        updateObj.set(getQueryModifiedDate(), new Date());
        updateObj.set(getQueryModifiedBy(), new Date());
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);
        return Optional.ofNullable(mongoOperations.findAndModify(query, updateObj, options, MemberCarrierDetails.class));
    }

    public void deleteByMemberCarrierDetails(String siteId, String queryValue, String type) {
        Query query = new Query();
        if ("ID".equals(type)) {
            query.addCriteria(Criteria.where(getQueryId()).is(queryValue).and(getQuerySiteId()).is(siteId));
        } else if ("MEMBER_NO".equals(type)) {
            query.addCriteria(Criteria.where(getQueryMemberNo()).is(queryValue).and(getQuerySiteId()).is(siteId));
        }
        mongoOperations.remove(query, MemberCarrierDetails.class);
    }

}
