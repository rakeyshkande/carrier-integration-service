package com.ftd.services.carrier.integration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftd.commons.logging.annotation.LogExecutionTime;
import com.ftd.services.carrier.integration.constants.AppConstants;
import com.ftd.services.carrier.integration.domain.ReturnStatus;
import com.ftd.services.carrier.integration.entity.CalyxCarrierEvent;
import com.ftd.services.carrier.integration.service.CalyxCarrierEventService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
//@RefreshScope
@RequestMapping(value = "/carrier-integration", produces = "application/hal+json")
public class CalyxCarrierEventController {

    @Autowired
    CalyxCarrierEventService calyxCarrierEventService;

    @LogExecutionTime
    @GetMapping(value = "/{siteId}/calyx-carrier-event", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CalyxCarrierEvent>> getCalyxCarrierEvents(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @RequestParam(name = "carrierId", required = true) String carrierId,
            @Validated @RequestParam(name = "carrierEvent", required = false) String carrierEvent) {
        log.info("Get list of calyx-carrier-events for site: {}", siteId);
        List<CalyxCarrierEvent> calyxCarrierEvents = calyxCarrierEventService.getCalyxCarrierEventsByCarrier(siteId, carrierId, carrierEvent);
        return ResponseEntity.ok(calyxCarrierEvents);
    }

    @LogExecutionTime
    @GetMapping(value = "/{siteId}/calyx-carrier-event/{calyxCarrierEventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CalyxCarrierEvent> getCalyxCarrierEventById(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @PathVariable(name = "calyxCarrierEventId", required = true) String calyxCarrierEventId) {
        log.info("Get list of calyx-carrier-events for site: {}", siteId);
        CalyxCarrierEvent calyxCarrierEvents = calyxCarrierEventService.getCalyxCarrierEventById(siteId, calyxCarrierEventId);
        return ResponseEntity.ok(calyxCarrierEvents);
    }

    @LogExecutionTime
    @PostMapping(value = "/{siteId}/calyx-carrier-event", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReturnStatus> saveCalyxCarrierEvent(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @RequestBody(required = true) CalyxCarrierEvent calyxCarrierEvent) {
        log.info("Save calyx-carrier-events under site: {} & info: {}", siteId, calyxCarrierEvent);
        CalyxCarrierEvent savedCalyxCarrierEvent = calyxCarrierEventService.saveCalyxCarrierEvent(siteId, calyxCarrierEvent);
        ReturnStatus status = new ReturnStatus();
        status.setId(savedCalyxCarrierEvent.getCalyxCarrierEventId());
        status.setSuccess(true);
        status.setMessage(AppConstants.DEFAULT_SUCCESS_MESSAGE);
        return new ResponseEntity<>(status, HttpStatus.CREATED);
    }

    @LogExecutionTime
    @DeleteMapping(value = "/{siteId}/calyx-carrier-event/{calyxCarrierEventId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReturnStatus> deleteCalyxCarrierEventById(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @PathVariable(name = "calyxCarrierEventId", required = true) String calyxCarrierEventId) {
        log.info("Delete calyx-carrier-events for site: {} & calyx carrier eventId id: {}", siteId, calyxCarrierEventId);
        String statusCode = calyxCarrierEventService.deleteCalyxCarrierEventById(siteId, calyxCarrierEventId);
        ReturnStatus status = new ReturnStatus();
        status.setMessage(statusCode);
        if ("SUCCESS".equals(statusCode)) {
            status.setSuccess(true);
            status.setMessage(AppConstants.DEFAULT_SUCCESS_MESSAGE);
        } else {
            status.setSuccess(false);
            status.setMessage(AppConstants.DEFAULT_FAILURE_MESSAGE);
        }
        return ResponseEntity.ok(status);
    }
}
