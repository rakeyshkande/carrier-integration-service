package com.ftd.services.carrier.integration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftd.commons.logging.annotation.LogExecutionTime;
import com.ftd.services.carrier.integration.constants.AppConstants;
import com.ftd.services.carrier.integration.domain.ReturnStatus;
import com.ftd.services.carrier.integration.entity.MemberCarrierDetails;
import com.ftd.services.carrier.integration.service.MemberCarrierService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
//@RefreshScope
@RequestMapping(value = "/carrier-integration", produces = "application/hal+json")
public class MemberCarrierController {

    @Autowired
    MemberCarrierService memberCarrierService;

    @LogExecutionTime
    @GetMapping(value = "/{siteId}/member-carrier-mapping", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MemberCarrierDetails>> getOrderTrackingDetails(
            @Validated @PathVariable(name = "siteId", required = true) String siteId) {
        log.info("Get list of Member - Carrier mappings for site: {}", siteId);
        List<MemberCarrierDetails> memberCarriers = memberCarrierService.getMemberCarrierDetails(siteId);
        return ResponseEntity.ok(memberCarriers);
    }

    @LogExecutionTime
    @GetMapping(value = "/{siteId}/member-carrier-mapping/{referenceNo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MemberCarrierDetails> getOrderTrackingDetailsById(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @PathVariable(name = "referenceNo", required = true) String referenceNo,
            @Validated @RequestParam(name = "type", required = true) String type) {
        log.info("Get Member - Carrier mappings for site: {} & member carrier details id: {}", siteId, referenceNo);
        MemberCarrierDetails memberCarrier = memberCarrierService.getMemberCarrierDetails(siteId, referenceNo, type);
        return ResponseEntity.ok(memberCarrier);
    }

    @LogExecutionTime
    @PostMapping(value = "/{siteId}/member-carrier-mapping", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReturnStatus> saveOrderTrackingDetails(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @RequestBody(required = true) MemberCarrierDetails memberCarrier) {
        log.info("Save Member - Carrier mappings under site: {} & member-carrier info: {}", siteId, memberCarrier);
        MemberCarrierDetails savedMemberCarrier = memberCarrierService.saveMemberCarrierDetails(siteId, memberCarrier);
        ReturnStatus status = new ReturnStatus();
        status.setId(savedMemberCarrier.getMemberCarrierDetailsId());
        status.setSuccess(true);
        status.setMessage(AppConstants.DEFAULT_SUCCESS_MESSAGE);
        return new ResponseEntity<>(status, HttpStatus.CREATED);
    }

    @LogExecutionTime
    @PutMapping(value = "/{siteId}/member-carrier-mapping/{memberNo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReturnStatus> updateOrderTrackingDetails(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @PathVariable(name = "memberNo", required = true) String memberNo,
            @Validated @RequestBody(required = true) MemberCarrierDetails memberCarrier) {
        log.info("Save Member - Carrier mappings under site: {} & member-carrier info: {}", siteId, memberCarrier);
        MemberCarrierDetails savedMemberCarrier = memberCarrierService.updateMemberCarrierDetails(siteId, memberNo, memberCarrier);
        ReturnStatus status = new ReturnStatus();
        status.setId(savedMemberCarrier.getMemberCarrierDetailsId());
        status.setSuccess(true);
        status.setMessage(AppConstants.DEFAULT_SUCCESS_MESSAGE);
        return ResponseEntity.ok(status);
    }

    @LogExecutionTime
    @DeleteMapping(value = "/{siteId}/member-carrier-mapping/{memberCarrierDetailsId}?carrierId={carrierid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReturnStatus> deleteOrderTrackingDetailsById(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @PathVariable(name = "memberCarrierDetailsId", required = true) String memberCarrierDetailsId,
            @Validated @RequestParam(name = "carrierId", required = false) String carrierId) {
        log.info("Get Member - Carrier mappings for site: {} & member carrier details id: {}", siteId, memberCarrierDetailsId);
        String statusCode = memberCarrierService.deleteOrderTrackingDetailsById(siteId, memberCarrierDetailsId, carrierId);
        ReturnStatus status = new ReturnStatus();
        status.setMessage(statusCode);
        if ("SUCCESS".equals(statusCode)) {
            status.setSuccess(true);
            status.setMessage(AppConstants.DEFAULT_SUCCESS_MESSAGE);
        } else {
            status.setSuccess(false);
            status.setMessage(AppConstants.DEFAULT_FAILURE_MESSAGE);
        }
        return ResponseEntity.ok(status);
    }

    @LogExecutionTime
    @DeleteMapping(value = "/{siteId}/member-carrier-mapping/{memberNo}?carrierId= {carrierid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReturnStatus> deleteOrderTrackingDetailsByMemberNo(
            @Validated @PathVariable(name = "siteId", required = true) String siteId,
            @Validated @PathVariable(name = "memberNo", required = true) String memberNo,
            @Validated @RequestParam(name = "carrierId", required = false) String carrierId) {
        log.info("Get Member - Carrier mappings for site: {} & member no: {}", siteId, memberNo);
        String statusCode = memberCarrierService.deleteOrderTrackingDetailsByMemberNo(siteId, memberNo, carrierId);
        ReturnStatus status = new ReturnStatus();
        status.setMessage(statusCode);
        if ("SUCCESS".equals(statusCode)) {
            status.setSuccess(true);
            status.setMessage(AppConstants.DEFAULT_SUCCESS_MESSAGE);
        } else {
            status.setSuccess(false);
            status.setMessage(AppConstants.DEFAULT_FAILURE_MESSAGE);
        }
        return ResponseEntity.ok(status);
    }
}
