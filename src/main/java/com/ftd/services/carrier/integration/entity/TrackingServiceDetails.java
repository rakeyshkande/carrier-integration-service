package com.ftd.services.carrier.integration.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrackingServiceDetails extends Audit {

    private Integer carrierId;
    private String accountCode;
    private String downloadTrackingInfo;
    private String trackingDownloadType;
    private String host;
    private String port;
    private String protocol;
    private String url;
    private String authType;
    private String userName;
    private String password;
    private String keyType;
    private String key;
    private String remoteDirectory;
    private String remoteFileName;
    private String localDirectory;
    private Date validFrom;
    private Date validTo;
}
