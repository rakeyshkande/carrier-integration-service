package com.ftd.services.carrier.integration.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public final class JsonUtil {

    private JsonUtil() {
        //not using
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String toJson(Object obj) {
        if (obj != null) {
            try {
                return objectMapper.writeValueAsString(obj);
            } catch (JsonProcessingException e) {
                LOGGER.error("Error while converting Object to Json: " + e);
            }
        }
        return null;
    }

    public static <T> T toObject(String json, Class<T> className) {
        if (!StringUtils.isEmpty(json) && className != null) {
            try {
                return objectMapper.readValue(json, className);
            } catch (IOException e) {
                LOGGER.error("Error while converting Json to Object: " + e);
            }
        }
        return null;
    }

}
